FROM node:16.9.0

# docker workdir
WORKDIR /home/usr/app

# copy files
COPY . .

# build the app
RUN npm ci && npm run build

# expose at port 3001
EXPOSE 3000

CMD ["npm", "run", "start"]
