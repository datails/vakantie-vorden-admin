import React from "react";
import TextField from "@material-ui/core/TextField";
import { useInput, required } from "react-admin";

const BoundedTextField = (props) => {
  const {
    input: { name, onChange, ...rest },
    meta: { touched, error },
    isRequired,
  } = useInput(props);

  return (
    <TextField
      name={name}
      label={props.label}
      onChange={onChange}
      error={!!(touched && error)}
      helperText={touched && error}
      required={isRequired}
      {...rest}
    />
  );
};
const LatLngInput = (props) => {
  const { source, ...rest } = props;

  return (
    <span>
      <BoundedTextField
        source="lat"
        label="Latitude"
        validate={required()}
        {...rest}
      />
      &nbsp;
      <BoundedTextField
        source="lng"
        label="Longitude"
        validate={required()}
        {...rest}
      />
    </span>
  );
};

export default LatLngInput;
