import React, { useState, useEffect } from "react";
import Moment from "moment";
import { extendMoment } from "moment-range";

import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";

import {
  Edit,
  SimpleForm,
  BooleanInput,
  NumberInput,
  DateInput,
  useDataProvider,
  Loading,
} from "react-admin";

import { DayPickerRangeController } from "react-dates";

const moment = extendMoment(Moment);

const handleInputSideBar = ({ props }) => {
  return (
    <Edit
      id={props.id}
      basePath="/properties"
      resource="properties"
      title="Beschikbaarheid"
    >
      <SimpleForm variant="standard">
        <h3>Beschikbaarheid wijzigen</h3>
        {props.endDate && (
          <>
            <DateInput
              label="Van"
              source={`availability.${`${props.startDate.format(
                "YYYY-MM-DD"
              )}-${props.endDate.format("YYYY-MM-DD")}`}.startDate`}
              defaultValue={props.startDate.format("YYYY-MM-DD")}
              disabled
            />
            <DateInput
              label="Tot"
              source={`availability.${`${props.startDate.format(
                "YYYY-MM-DD"
              )}-${props.endDate.format("YYYY-MM-DD")}`}.endDate`}
              defaultValue={props.endDate.format("YYYY-MM-DD")}
              disabled
            />
            <BooleanInput
              label="Beschikbaar"
              source={`availability.${`${props.startDate.format(
                "YYYY-MM-DD"
              )}-${props.endDate.format("YYYY-MM-DD")}`}.isAvailable`}
              defaultValue={true}
            />
            <NumberInput
              label="Prijs"
              source={`availability.${`${props.startDate.format(
                "YYYY-MM-DD"
              )}-${props.endDate.format("YYYY-MM-DD")}`}.price`}
            />
          </>
        )}
      </SimpleForm>
    </Edit>
  );
};

const isDateBlocked = (start, end, dates) => {
  const dateFormat = "YYYY-MM-DD";
  const diff = moment(end).diff(start, "days") + 1;

  for (let i = 0; i < diff; i++) {
    const checkDate = moment(start).add(i, "d").format(dateFormat);

    const item = dates.find((d) => {
      return d.format("YYYY-MM-DD") === checkDate;
    });

    if (item) {
      return true;
    }
  }

  return false;
};

const CustomDataRangePicker = ({ props }) => {
  const dataProvider = useDataProvider();
  const [loading, setLoading] = useState(true);
  const [availability, setAvailability] = useState([]);
  const [state, setState] = useState({
    fullscreen: true,
    direction: "left",
    dateFormat: "MM/DD/YYYY",
    small: false,
    block: true,
    numMonths: 1,
    minimumNights: 7,
    id: props.id,
  });
  const [focusedInput, setFocusedInput] = useState("startDate");

  const [date, setDate] = useState({
    startDate: null,
    endDate: null,
  });

  useEffect(() => {
    dataProvider
      .getOne("properties", {
        id: props.id,
      })
      .then(({ data }) => {
        let BLOCKED_DATES = [];

        for (const dateObj in data?.data?.availability) {
          const item = data.data.availability[dateObj];

          if (!item.isAvailable) {
            const range = moment.range(item.startDate, item.endDate);
            const acc = Array.from(range.by("day"));

            BLOCKED_DATES = BLOCKED_DATES.concat(acc);
          }
        }

        if (availability.length === 0) {
          setAvailability(BLOCKED_DATES);
        }

        if (loading) {
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
      });

    dataProvider
      .getList("reservations", {
        pagination: {},
        sort: {},
        filter: {},
      })
      .then(({ data }) => {
        let BLOCKED_DATES = [];

        for (const dateObj in data?.data?.availability) {
          const item = data.data.availability[dateObj];

          if (!item.isAvailable) {
            const range = moment.range(item.startDate, item.endDate);
            const acc = Array.from(range.by("day"));

            BLOCKED_DATES = BLOCKED_DATES.concat(acc);
          }
        }

        if (availability.length === 0) {
          setAvailability(BLOCKED_DATES);
        }

        if (loading) {
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
      });
  });

  if (loading) return <Loading />;

  return (
    <DayPickerRangeController
      navPosition="navPositionBottom"
      keepOpenOnDateSelect={true}
      startDate={date.startDate} // momentPropTypes.momentObj or null,
      date={moment()}
      endDate={date.endDate} // momentPropTypes.momentObj or null,
      // endDateId="unique_end_date_id"
      onDatesChange={({ startDate, endDate }) => {
        if (!isDateBlocked(startDate, endDate, availability.data)) {
          setDate({
            startDate,
            endDate,
          });
        } else {
          setDate({
            startDate,
          });
        }
      }}
      focusedInput={focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
      onFocusChange={(fcsInput) => {
        setFocusedInput(fcsInput);
      }}
      renderDayContents={(day, modifiers) => {
        day._locale._weekdaysMin = ["SU", "MO", "TU", "WE", "TH", "FR", "SA"];
        return day.format("D");
      }}
      displayFormat={state.dateFormat}
      numberOfMonths={state.numMonths || 2}
      block={state.block}
      small={state.small}
      withFullScreenPortal={state.fullscreen}
      anchorDirection={state.direction}
      orientation={state.orientation}
      isDayBlocked={(day) => {
        return availability.filter((d) => d.isSame(day, "day")).length > 0;
      }}
      orientation="horizontal"
      daySize={100}
      showInputs
      calendarInfoPosition="after"
      renderCalendarInfo={() =>
        handleInputSideBar({
          props: {
            id: props.id,
            ...date,
          },
        })
      }
    />
  );
};

export default CustomDataRangePicker;
