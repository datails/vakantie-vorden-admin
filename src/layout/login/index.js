import * as React from "react";
import { useState } from "react";
import PropTypes from "prop-types";
import { Field, withTypes } from "react-final-form";
import { useLocation } from "react-router-dom";
import {
  Avatar,
  Button,
  Card,
  CardActions,
  CircularProgress,
  TextField,
  Grid,
  CardContent,
  Typography,
  Link,
} from "@material-ui/core";
import { Notification, useTranslate, useLogin, useNotify } from "react-admin";
import useStyles from "./login.styles";
import withRoot from "../theme/withRoot";
import CreateUser from "./createNewUser";

const renderInput = ({
  meta: { touched, error } = { touched: false, error: undefined },
  input: { ...inputProps },
  ...props
}) => (
  <TextField
    error={!!(touched && error)}
    helperText={touched && error}
    {...inputProps}
    {...props}
    fullWidth
  />
);

const { Form } = withTypes();

const Login = () => {
  const [loading, setLoading] = useState(false);
  const [comp, setComp] = useState("login");
  const translate = useTranslate();
  const classes = useStyles();
  const notify = useNotify();
  const login = useLogin();
  const location = useLocation();

  const handleSubmit = (auth) => {
    setLoading(true);
    login(auth, location.state ? location.state.nextPathname : "/").catch(
      (error) => {
        setLoading(false);
        notify(
          typeof error === "string"
            ? error
            : typeof error === "undefined" || !error.message
            ? "ra.auth.sign_in_error"
            : error.message,
          "warning"
        );
      }
    );
  };

  const validate = (values) => {
    const errors = {};
    if (!values.username) {
      errors.username = translate("ra.validation.required");
    }
    if (!values.password) {
      errors.password = translate("ra.validation.required");
    }
    return errors;
  };

  if (comp !== "login") {
    return <CreateUser />;
  }

  return (
    <Form
      onSubmit={handleSubmit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} noValidate>
          <Grid
            container
            className={classes.root}
            justify="center"
            alignContent="center"
            alignItems="center"
          >
            <Grid item xs={12} sm={6} md={4} xl={3}>
              <Card className={classes.card}>
                <div className={classes.avatar}>
                  <Avatar
                    className={classes.icon}
                    sizes={80}
                    src={require("../../assets/logo.svg")}
                  />
                </div>
                <div className={classes.form}>
                  <div className={classes.input}>
                    <Field
                      autoFocus
                      name="username"
                      component={renderInput}
                      label="Gebruikersnaam"
                      disabled={loading}
                    />
                  </div>
                  <div className={classes.input}>
                    <Field
                      name="password"
                      component={renderInput}
                      label={"Wachtwoord"}
                      type="password"
                      disabled={loading}
                    />
                  </div>
                </div>
                <CardActions className={classes.actions}>
                  <Button
                    variant="contained"
                    type="submit"
                    color="primary"
                    disabled={loading}
                    fullWidth
                    className={classes.button}
                  >
                    {loading && <CircularProgress size={25} thickness={2} />}
                    Inloggen
                  </Button>
                </CardActions>
                {
                  /** TEMP: disable create own accounts. Not supported currently. */
                }
                {/* <CardContent>
                  <Link
                    className={classes.createAccount}
                    onClick={() => setComp("create")}
                  >
                    Create account
                  </Link>
                </CardContent> */}
              </Card>
              <Notification />
            </Grid>
          </Grid>
        </form>
      )}
    />
  );
};

Login.propTypes = {
  authProvider: PropTypes.func,
  previousRoute: PropTypes.string,
};

export default withRoot(Login);
