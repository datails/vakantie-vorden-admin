import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0 auto",
      width: "100%",
      maxWidth: 1200,
      padding: "40px 0",
    },
    content: {
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      fontSize: "1.1rem",
      textAlign: "justify",
      fontWeight: 300,
      lineHeight: 1.8,
      padding: "40px",
    },
  })
);
